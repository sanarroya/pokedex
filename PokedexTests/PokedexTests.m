//
//  PokedexTests.m
//  PokedexTests
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Mantle/Mantle.h>
#import "Ability.h"
#import "Description.h"
#import "DescriptionInfo.h"
#import "Evolution.h"
#import "Move.h"
#import "Type.h"
#import "ObjectModel.h"
#import "PokemonResponseModel.h"
#import "Pokemon.h"
#import "APIManager.h"
#import "PokemonDetail.h"
#import "DescriptionInfo.h"
@interface PokedexTests : XCTestCase

@property (nonatomic, strong)NSDictionary *abilityDictionary;
@property (nonatomic, strong)NSDictionary *descriptionDictionary;
@property (nonatomic, strong)NSDictionary *evolutionDictionary;
@property (nonatomic, strong)NSDictionary *moveDictionary;
@property (nonatomic, strong)NSDictionary *typeResponseModel;
@property (nonatomic, strong)NSDictionary *pokemonDetailDictionary;
@property (nonatomic, strong)NSDictionary *pokemonDictionary;
@property (nonatomic, strong)NSDictionary *objectModelDictionary;
@property (nonatomic, strong)NSDictionary *pokemonResponseModelDictionary;

@end

@implementation PokedexTests

- (void)setUp {
    [super setUp];
    self.abilityDictionary = @{@"name": @"name"};
    self.pokemonDictionary = @{
                               @"name": @"name",
                               @"resourceUri": @"resource_uri"
                               };
    self.objectModelDictionary = @{
                                   @"pokemons": self.pokemonDictionary
                                   };
    self.pokemonResponseModelDictionary = @{
                                            @"objects": self.objectModelDictionary
                                            };
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAbility {
    NSError *error;
    Ability *ability = [[Ability alloc] initWithDictionary:self.abilityDictionary error:&error];
    XCTAssertNotNil(ability, @"Should be able to create an Ability instance");
}

- (void)testDescription {
    Description *description = [[Description alloc] init];
    XCTAssertNotNil(description, @"Should be able to create a Description instance");
}

- (void)testDescriptionInfo {
    DescriptionInfo *descriptionInfo = [[DescriptionInfo alloc] init];
    XCTAssertNotNil(descriptionInfo, @"Should be able to create DescriptionInfo instance");
}

- (void)testEvolution {
    Evolution *evolution = [[Evolution alloc] init];
    XCTAssertNotNil(evolution, @"Should be able to create evolution instance");
}

- (void)testMove {
    Move *move = [[Move alloc] init];
    XCTAssertNotNil(move, @"Should be able to create move instance");
}

- (void)testType {
    Type *type = [[Type alloc] init];
    XCTAssertNotNil(type, @"Should be able to create type instance");
}

- (void)testPokemonDetail {
    PokemonDetail *pokemonDetail = [[PokemonDetail alloc] init];
    XCTAssertNotNil(pokemonDetail, @"Should be able to create PokemonDetail instance");
}

- (void)testPokemon {
    Pokemon *pokemon = [[Pokemon alloc] init];
    XCTAssertNotNil(pokemon, @"Should be able to create pokemon instance");
}

- (void)testObjectModelExists {
    ObjectModel *objectModel = [[ObjectModel alloc] init];
    XCTAssertNotNil(objectModel, @"Should be able to create pokemon instance");
}

- (void)testPokemonResponseModel {
    NSError *error;
   
    PokemonResponseModel *pokemonResponseModel = [MTLJSONAdapter modelOfClass:[PokemonResponseModel class] fromJSONDictionary:self.pokemonResponseModelDictionary error:&error];
    XCTAssertNotNil(pokemonResponseModel, @"Should be able to create pokemon instance");
}

@end
