//
//  PokemonDetailsTableViewControllerTests.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/5/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PokemonDetailsTableViewController.h"

@interface PokemonDetailsTableViewControllerTests : XCTestCase

@property (nonatomic) PokemonDetailsTableViewController *detailsVCTest;

@end

@implementation PokemonDetailsTableViewControllerTests

- (void)setUp {
    [super setUp];
    self.detailsVCTest =[[PokemonDetailsTableViewController alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testInitArrays {
    self.detailsVCTest.initArrays;
    XCTAssertNotNil(self.name);
}

@end
