# README #

Here are the libraries used in this project

### AFNetworking ###

This library handles all the networking of the application, I use it because it transform the NSData response to JSON and all the request are Asynchronous.

[AFNetworking documentation](https://github.com/AFNetworking/AFNetworking)

### Mantle ###

This library maps the JSON response into the model objects it simplifies writing data models for interacting with APIs that use JSON as their data exchange format.

[Mantle documentation](https://github.com/Mantle/Mantle)

### KVNProgress ###

Library in charge of presenting the user a loading view while the request is in progress.

[KVNProgress](https://github.com/kevin-hirsch/KVNProgress)


### Tests ###

The UI test was made with XCTestCase provided by apple.