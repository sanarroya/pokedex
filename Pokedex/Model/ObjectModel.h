//
//  Object.h
//  Pokedex
//
//  Created by URpin on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "Pokemon.h"
@interface ObjectModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray *pokemons;

@end
