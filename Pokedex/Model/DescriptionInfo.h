//
//  DescriptionInfo.h
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DescriptionInfo : MTLModel <MTLJSONSerializing>

@property(nonatomic, copy) NSString *descriptionInfo;

@end
