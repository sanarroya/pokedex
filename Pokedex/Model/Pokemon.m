//
//  Pokemon.m
//  Pokedex
//
//  Created by URpin on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "Pokemon.h"

@implementation Pokemon

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"resourceUri": @"resource_uri"
             };
}
@end
