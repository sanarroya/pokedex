//
//  PokemonResponseModel.m
//  Pokedex
//
//  Created by URpin on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "PokemonResponseModel.h"

@class ObjectModel;

@implementation PokemonResponseModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objects": @"objects"
             };
}

+ (NSValueTransformer *)objectsJSONTransformer {
    return  [MTLJSONAdapter arrayTransformerWithModelClass:ObjectModel.class];
}
@end
