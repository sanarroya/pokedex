//
//  Move.h
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Move : MTLModel <MTLJSONSerializing>

@property(nonatomic, copy) NSString *name;

@end
