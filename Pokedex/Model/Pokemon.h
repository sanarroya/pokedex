//
//  Pokemon.h
//  Pokedex
//
//  Created by URpin on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Pokemon : MTLModel <MTLJSONSerializing>

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *resourceUri;

@end
