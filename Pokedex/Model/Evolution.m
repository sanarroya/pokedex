//
//  Evolution.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "Evolution.h"

@implementation Evolution

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"to": @"to",
             @"resourceUri": @"resource_uri"
             };
}
@end
