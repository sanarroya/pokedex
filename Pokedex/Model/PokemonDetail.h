//
//  Pokemon.h
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "Ability.h"
#import "Evolution.h"
#import "Description.h"
#import "Type.h"
#import "Move.h"

@interface PokemonDetail : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSNumber *nationalId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *created;
@property (nonatomic, copy) NSString *modified;
@property (nonatomic, copy) NSString *height;
@property (nonatomic, copy) NSString *weight;
@property (nonatomic, copy) NSString *maleFemaleRatio;
@property (nonatomic) NSNumber *catchRate;
@property (nonatomic) NSNumber *hp;
@property (nonatomic) NSNumber *attack;
@property (nonatomic) NSNumber *defense;
@property (nonatomic) NSNumber *speed;
@property (nonatomic, copy) NSArray *abilities;
@property (nonatomic, copy) NSArray *evolutions;
@property (nonatomic, copy) NSArray *descriptions;
@property (nonatomic, copy) NSArray *moves;
@property (nonatomic, copy) NSArray *types;

@end
