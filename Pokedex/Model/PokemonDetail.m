//
//  Pokemon.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "PokemonDetail.h"

@class Ability;
@class Description;
@class Evolution;
@class Move;
@class Type;

@implementation PokemonDetail

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"nationalId": @"national_id",
             @"name": @"name",
             @"created": @"created",
             @"modified": @"modified",
             @"height": @"height",
             @"weight": @"weight",
             @"maleFemaleRatio": @"male_female_ratio",
             @"catchRate": @"catch_rate",
             @"hp": @"hp",
             @"attack": @"attack",
             @"defense": @"defense",
             @"speed": @"speed",
             @"abilities": @"abilities",
             @"evolutions": @"evolutions",
             @"descriptions": @"descriptions",
             @"moves": @"moves",
             @"types": @"types"
             };
}

+ (NSValueTransformer *)abilitiesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Ability.class];
    
}

+ (NSValueTransformer *)evolutionsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Evolution.class];
    
}

+ (NSValueTransformer *)descriptionsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Description.class];
    
}

+ (NSValueTransformer *)movesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Move.class];
    
}

+ (NSValueTransformer *)typesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Type.class];
    
}

@end
