//
//  Ability.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "Ability.h"

@implementation Ability

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name"
             };
}
@end
