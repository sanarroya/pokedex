//
//  DescriptionInfo.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "DescriptionInfo.h"

@implementation DescriptionInfo

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"descriptionInfo": @"description",
             };
}

@end
