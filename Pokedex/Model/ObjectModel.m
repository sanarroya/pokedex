//
//  Object.m
//  Pokedex
//
//  Created by URpin on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "ObjectModel.h"

@class Pokemon;

@implementation ObjectModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"pokemons": @"pokemon"
             };
}

+ (NSValueTransformer *)pokemonsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Pokemon.class];
}
@end
