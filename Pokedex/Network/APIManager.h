//
//  APIManager.h
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "SessionManager.h"
#import "PokemonResponseModel.h"
#import "PokemonDetail.h"
#import "DescriptionInfo.h"
#import <Mantle/Mantle.h>

@interface APIManager : SessionManager

- (NSURLSessionDataTask *) getPokedexWithSuccess:(void (^)(PokemonResponseModel *responseModel))success failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)getPokemonDetails:(NSString *)endPoint success:(void(^)(PokemonDetail *responseModel))success failure:(void (^)(NSError *error))failure;
- (NSURLSessionDataTask *)getPokemonDescriptions:(NSString *)endPoint success:(void(^)(DescriptionInfo *responseModel))success failure:(void (^)(NSError *error))failure;
@end
