//
//  APIManager.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "APIManager.h"

static NSString *const kPokeDexEndpoint = @"api/v1/pokedex";

@implementation APIManager

- (NSURLSessionDataTask *) getPokedexWithSuccess:(void (^)(PokemonResponseModel *responseModel))success failure:(void (^)(NSError *error))failure{
    
    return [self GET:kPokeDexEndpoint
          parameters:nil
             success:^(NSURLSessionDataTask *task, id  responseObject) {
                 NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                 NSError *error;
                 PokemonResponseModel *list = [MTLJSONAdapter modelOfClass:PokemonResponseModel.class fromJSONDictionary:responseDictionary error:&error];
                 success(list);
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                failure(error);
    }];
    
}

- (NSURLSessionDataTask *)getPokemonDetails:(NSString *)endPoint success:(void(^)(PokemonDetail *responseModel))success failure:(void (^)(NSError *error))failure {
    return [self GET:endPoint
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                 NSError *error;
                 PokemonDetail *detail = [MTLJSONAdapter modelOfClass:PokemonDetail.class fromJSONDictionary:responseDictionary error:&error];
                 success(detail);
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 failure(error);
    }];
}

- (NSURLSessionDataTask *)getPokemonDescriptions:(NSString *)endPoint success:(void(^)(DescriptionInfo *responseModel))success failure:(void (^)(NSError *error))failure {
    return [self GET:endPoint
          parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                 NSError *error;
                 DescriptionInfo *description = [MTLJSONAdapter modelOfClass:DescriptionInfo.class fromJSONDictionary:responseDictionary error:&error];
                 success(description);
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 failure(error);
             }];
}

@end
