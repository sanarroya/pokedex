//
//  PokemonDetailsViewController.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "PokemonDetailsViewController.h"

@interface PokemonDetailsViewController ()

@end

@implementation PokemonDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
