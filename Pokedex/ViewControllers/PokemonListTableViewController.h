//
//  PokemonListTableViewController.h
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"
#import "PokemonResponseModel.h"
#import <KVNProgress/KVNProgress.h>
#import "PokemonDetailsTableViewController.h"
@interface PokemonListTableViewController : UITableViewController

@end
