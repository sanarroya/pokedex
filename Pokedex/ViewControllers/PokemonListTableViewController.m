//
//  PokemonListTableViewController.m
//  Pokedex
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import "PokemonListTableViewController.h"

@interface PokemonListTableViewController ()

@property (nonatomic, strong) NSArray *pokemons;

@end

@implementation PokemonListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [self getPokemons];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.pokemons count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Pokemon *pokemon = (Pokemon *)[self.pokemons objectAtIndex:indexPath.row];
    cell.textLabel.text = pokemon.name;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Pokemon *pokemon = (Pokemon *)[self.pokemons objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"DetailsView" sender:pokemon];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[PokemonDetailsTableViewController class]]) {
        PokemonDetailsTableViewController *pokemonDetailsVC = segue.destinationViewController;
        Pokemon *pokemonToSend =(Pokemon *)sender;
        pokemonDetailsVC.pokemon = pokemonToSend;
    }
}


- (void) getPokemons {
    [KVNProgress showWithStatus:@"Loading Pokémons..."];
    [[APIManager sharedManager] getPokedexWithSuccess:^(PokemonResponseModel *responseModel) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *objects = responseModel.objects;
            ObjectModel *object = [objects firstObject];
            self.pokemons = object.pokemons;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [KVNProgress dismiss];
            });
        });
    } failure:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

@end
