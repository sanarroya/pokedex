//
//  PokemonDetailsTableViewController.h
//  
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//
//

#import <UIKit/UIKit.h>
#import "Pokemon.h"
#import "APIManager.h"
#import "PokemonDetail.h"
#import "DescriptionInfo.h"
#import <KVNProgress/KVNProgress.h>


@interface PokemonDetailsTableViewController : UITableViewController {
    NSArray *sections;
}

@property(nonatomic, strong)Pokemon *pokemon;
@property (nonatomic, strong) NSMutableArray *details;
@property (nonatomic, strong) NSArray *name;
@property (nonatomic, strong) NSArray *finalDescription;
@property (nonatomic, strong) NSArray *generalInfo;
@property (nonatomic, strong) NSArray *history;
@property (nonatomic, strong) NSArray *abilities;
@property (nonatomic, strong) NSArray *moves;
@property (nonatomic, strong) NSArray *types;
@property (nonatomic, strong) NSArray *evolutions;
@property (nonatomic, strong) NSArray *descriptions;

- (NSString *)whichDescription;
- (void) initArrays;
@end
