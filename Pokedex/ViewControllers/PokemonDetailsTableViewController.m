//
//  PokemonDetailsTableViewController.m
//  
//
//  Created by Santiago Avila Arroyave on 12/2/15.
//
//

#import "PokemonDetailsTableViewController.h"

@interface PokemonDetailsTableViewController ()

@end

@implementation PokemonDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    sections = @[@"NAME", @"DESCRIPTION", @"GENERAL INFORMATION", @"ABILITIES", @"MOVES", @"TYPES", @"EVOLUTIONS", @"HISTORY"];
    self.navigationItem.title = [NSString stringWithFormat:@"%@ details",self.pokemon.name];
    [KVNProgress showWithStatus:@"Loading Pokémon description..."];
    [self loadDetails:self.pokemon.resourceUri];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionContents = [[self details] objectAtIndex:section];
    NSInteger rows = [sectionContents count];
    return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [sections objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSString *contentForThisRow = [NSString new];
    NSArray *sectionContents = [[self details] objectAtIndex:[indexPath section]];
    
    if ([indexPath section] <= 2 || [indexPath section] == 7) {
        contentForThisRow = [sectionContents objectAtIndex:[indexPath row]];
    }else if ([indexPath section] == 3) {
        Ability *ability = (Ability *)[sectionContents objectAtIndex:[indexPath row]];
        contentForThisRow = ability.name;
    }else if ([indexPath section] == 4) {
        Move *move = (Move *)[sectionContents objectAtIndex:[indexPath row]];
        contentForThisRow = move.name;
    }else if ([indexPath section] == 5) {
        Type *type = (Type *)[sectionContents objectAtIndex:[indexPath row]];
        contentForThisRow = type.name;
    }else if ([indexPath section] == 6) {
        Evolution *evolution = (Evolution *)[sectionContents objectAtIndex:[indexPath row]];
        contentForThisRow = evolution.to;
    }

    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = contentForThisRow;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 6) {
        [KVNProgress showWithStatus:@"Loading Pokémon Evolution description..."];
        NSArray *sectionContents = [[self details] objectAtIndex:[indexPath section]];
        Evolution *evolution = (Evolution *)[sectionContents objectAtIndex:[indexPath row]];
        self.navigationItem.title = [NSString stringWithFormat:@"%@ details",evolution.to];
        
        [self loadDetails:evolution.resourceUri];
    }
}

- (void)loadDetails:(NSString *)resourceUri {
    
    [[APIManager sharedManager] getPokemonDetails:resourceUri success:^(PokemonDetail *responseModel) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self setUpArrays:responseModel];
            [self loadDescription];
            
        });
        
    } failure:^(NSError *error) {
        NSInteger statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
        if (statusCode == 404) {
            [KVNProgress showErrorWithStatus:@"Description not found" completion:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
    }];

}

- (void)loadDescription {
    [[APIManager sharedManager] getPokemonDescriptions:[self whichDescription] success:^(DescriptionInfo *responseModel) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@" Description %@", responseModel);
            self.finalDescription = [NSArray arrayWithObjects:
                                    responseModel.descriptionInfo,
                                    nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadTableData];
                [KVNProgress dismiss];
            });
        });

    } failure:^(NSError *error) {
        NSInteger statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
        if (statusCode == 404) {
            [KVNProgress showErrorWithStatus:@"Description not found" completion:^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
    }];
}

- (NSString *)whichDescription {
    int random = arc4random_uniform((int)[self.descriptions count]);
    Description *description = [self.descriptions objectAtIndex:random];
    NSLog(@"%@", description.resourceUri);
    return description.resourceUri;
}

- (void) setUpArrays:(PokemonDetail *)responseModel {
    [self initArrays];
    self.abilities = responseModel.abilities;
    self.descriptions = responseModel.descriptions;
    self.evolutions = responseModel.evolutions;
    self.moves = responseModel.moves;
    self.types = responseModel.types;
    self.name = [NSArray arrayWithObjects:
                 responseModel.name,
                 nil];
    self.generalInfo = [NSArray arrayWithObjects:
                        [NSString stringWithFormat:@"National ID: %@",responseModel.nationalId],
                        [NSString stringWithFormat:@"Hp: %@",responseModel.hp],
                        [NSString stringWithFormat:@"Attack: %@",responseModel.attack],
                        [NSString stringWithFormat:@"Defense: %@",responseModel.defense],
                        [NSString stringWithFormat:@"Speed: %@",responseModel.speed],
                        [NSString stringWithFormat:@"Height: %@",responseModel.height],
                        [NSString stringWithFormat:@"Weight: %@",responseModel.weight],
                        [NSString stringWithFormat:@"M/F Ratio: %@",responseModel.maleFemaleRatio],
                        [NSString stringWithFormat:@"Catch Rate: %@",responseModel.catchRate],
                        nil];
    NSLog(@"%@", self.generalInfo);
    self.history = [NSArray arrayWithObjects:
                    [NSString stringWithFormat:@"Created: %@",responseModel.created],
                    [NSString stringWithFormat:@"Modified: %@",responseModel.modified],
                    nil];
}

- (void) initArrays {
    self.name = [[NSMutableArray alloc]init];
    self.generalInfo = [[NSMutableArray alloc]init];
    self.history = [[NSMutableArray alloc]init];
    self.details = [[NSMutableArray alloc]init];
    
}

- (void) reloadTableData {
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:
                             self.name,
                             self.finalDescription,
                             self.generalInfo,
                             self.abilities,
                             self.moves,
                             self.types,
                             self.evolutions,
                             self.history,
                             nil];

    [self setDetails:array];
    [self.tableView reloadData];
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];


}

@end
