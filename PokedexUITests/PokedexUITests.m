//
//  PokedexUITests.m
//  PokedexUITests
//
//  Created by Santiago Avila Arroyave on 12/1/15.
//  Copyright © 2015 Santiago Avila Arroyave. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface PokedexUITests : XCTestCase

@end

@implementation PokedexUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void) testPokedex {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElementQuery *tablesQuery = app.tables;
//    XCTAssertEqual(tablesQuery.cells.count, 778);
    [tablesQuery.staticTexts[@"rattata"] tap];
    [tablesQuery.staticTexts[@"Speed: 72"] swipeUp];
    [tablesQuery.staticTexts[@"M/F Ratio: "] tap];
    [tablesQuery.staticTexts[@"Captivate"] tap];
    
    XCUIElement *raticateStaticText = [[[[tablesQuery.cells containingType:XCUIElementTypeStaticText identifier:@"Raticate"] childrenMatchingType:XCUIElementTypeStaticText] matchingIdentifier:@"Raticate"] elementBoundByIndex:0];
    [raticateStaticText swipeUp];
    [raticateStaticText tap];
    [app.navigationBars[@"Raticate details"].buttons[@"Pokemon List"] tap];

}

@end
